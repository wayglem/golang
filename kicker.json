{
  "name": "Go",
  "description": "Build and test with [Go](https://golang.org/).",
  "template_path": "templates/gitlab-ci-golang.yml",
  "kind": "build",
  "variables": [
    {
      "name": "GO_IMAGE",
      "description": "The Docker image used to run Go (build+test or build only) - **set the version required by your project**",
      "default": "golang:buster"
    },
    {
      "name": "GO_PROJECT_DIR",
      "description": "Go project root directory",
      "default": ".",
      "advanced": true
    },
    {
      "name": "GOPROXY",
      "description": "URL of Go module proxy (see [Go env](https://golang.org/cmd/go/#hdr-Environment_variables))",
      "advanced": true
    },
    {
      "name": "GO_TEST_IMAGE",
      "description": "Specific Docker image used to run Go tests (as a separate job)",
      "advanced": true
    },
    {
      "name": "GO_BUILD_FLAGS",
      "description": "Flags used by the [go build command](https://pkg.go.dev/cmd/go#hdr-Compile_packages_and_dependencies)",
      "default": "-mod=readonly",
      "advanced": true
    },
    {
      "name": "GO_BUILD_PACKAGES",
      "description": "Packages to build with the [go build command](https://pkg.go.dev/cmd/go#hdr-Compile_packages_and_dependencies)",
      "default": "./..."
    },
    {
      "name": "GO_TARGET_OS",
      "description": "The `$GOOS` target [see available values](https://gist.github.com/asukakenji/f15ba7e588ac42795f421b48b8aede63)\n\nFallbacks to default `$GOOS` from the Go Docker image",
      "advanced": true
    },
    {
      "name": "GO_TARGET_ARCH",
      "description": "The `$GOARCH` target [see available values](https://gist.github.com/asukakenji/f15ba7e588ac42795f421b48b8aede63)\n\nFallbacks to default `$GOARCH` from the Go Docker image",
      "advanced": true
    },
    {
      "name": "GO_TEST_FLAGS",
      "description": "Flags used by the [go test command](https://pkg.go.dev/cmd/go#hdr-Test_packages)",
      "default": "-mod=readonly -v -race",
      "advanced": true
    },
    {
      "name": "GO_TEST_PACKAGES",
      "description": "Packages to test with the [go test command](https://pkg.go.dev/cmd/go#hdr-Test_packages)",
      "default": "./..."
    },
    {
      "name": "GO_LIST_ARGS",
      "description": "Arguments used by the list command",
      "default": "list -u -m -mod=readonly -json all",
      "advanced": true
    }
  ],
  "features": [
    {
      "id": "golangci-lint",
      "name": "GolangCI-Lint",
      "description": "[GolangCI-Lint](https://github.com/golangci/golangci-lint) analysis",
      "disable_with": "GO_CI_LINT_DISABLED",
      "variables": [
        {
          "name": "GO_CI_LINT_IMAGE",
          "description": "The Docker image used to run `golangci-lint`",
          "default": "golangci/golangci-lint:latest-alpine"
        },
        {
          "name": "GO_CI_LINT_ARGS",
          "description": "`golangci-lint` [command line arguments](https://github.com/golangci/golangci-lint#command-line-options)",
          "default": "-E gosec,goimports ./...",
          "advanced": true
        }
      ]
    },
    {
      "id": "go-mod-outdated",
      "name": "Go-mod-outdated",
      "description": "[Go-mod-outdated](https://github.com/psampaz/go-mod-outdated) analysis",
      "variables": [
        {
          "name": "GO_MOD_OUTDATED_ARGS",
          "description": "`god-mod-outdated` [command line arguments](https://github.com/psampaz/go-mod-outdated#usage",
          "default": "-update -direct -style markdown -ci",
          "advanced": true
        }
      ]
    }
  ]
}
